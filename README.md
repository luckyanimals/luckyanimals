# README #



### What is this repository for? ###

* This repo is for a prototype of a social network for animal adoption developed with Ruby on Rails. It contains documentation, figures and the code itself. 


### How do I get set up? ###

* First you should get your Rails enviroment set up done before test the app.
* The Ruby version used is 2.0.0. Check the Gemfile to get the gem versions.
* Database used until now is MySql, but it will be PostgreSql.

### Contribution guidelines ###

* To contribute in any way to this project please talk to owner aranhaqg@gmail.com

### Who do I talk to? ###

* Repo owner or admin (aranhaqg@gmail.com)