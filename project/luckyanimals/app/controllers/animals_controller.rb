class AnimalsController < ApplicationController
  before_action :set_animal, only: [:show , :adopt, :destroy]

  def index
    @animals = Animal.where("user_id != ?" , @current_user).where(adopted: false)
    render "index"
  end

  def my_animals
    @animals = Animal.where(user: @current_user)

    render "my_animals"
  end

  def new
    @animal = Animal.new
  end

  def show
  end

  def adopt
    @animal.adopted = true
    @animal.user = @current_user
    if @animal.save
      
      redirect_to animal_url, notice: @animal.name + " was adopted. The world is getting better because of you. :)"
    
    else
      redirect_to animal_url, alert: @animal.name + " coud not be adopted."
    end
  end

  def create
    @animal = Animal.new(animal_params)
    @animal.adopted = false
    @animal.user = @current_user
    

    unless  params[:post_attachments].blank?
      #  "Please add a picture for your pet."
      @animal.posts = Array.new 
      post = Post.new
      post.description = "New animal added for adoption."
      
      post.post_attachments = Array.new

      params[:post_attachments]['image'].each do |a|
        attachment = PostAttachment.new
        attachment.image = a
        post.post_attachments.push attachment
      end

      @animal.posts.push post
      
    end

    if @animal.save
      redirect_to my_animals_url, notice: "New animal added for adoption!"
    else
      render "new"
    end
    
  end

  def destroy
    @animal.destroy
    redirect_to animals_url, notice: @animal.name + " was deleted."
  end

  def animal_params

    params.require(:animal)
          .permit(:name, :birthday, :gender, :species, :size, :coat_color, :eyes_color, :neutered, :adopted, :post_attachments)
            
  end

  def set_animal
    @animal = Animal.find(params[:id])
  end

end