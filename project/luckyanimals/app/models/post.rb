class Post < ActiveRecord::Base
	belongs_to :animal
	has_many :post_attachments, :dependent => :destroy
    
    accepts_nested_attributes_for :post_attachments, :allow_destroy => true
end
