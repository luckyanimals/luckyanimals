class Animal < ActiveRecord::Base
	belongs_to :user
	has_many :posts
	has_many :post_attachments, through: :posts

	validates :name, presence: true
	validates :gender, presence: true
	validates :birthday, presence: true
	validates :species, presence: true
	validates :size, presence: true
	validates :coat_color, presence: true
	validates :eyes_color, presence: true
	validates :neutered, presence: true

	validates :posts, presence:true
	
	belongs_to :user
	has_many :posts, :dependent => :destroy

	accepts_nested_attributes_for :posts, :allow_destroy => true

	def birthday=(date)
     begin
       parsed = Date.strptime(date,'%m/%d/%Y')
       super parsed
     rescue
       date         
     end
  end

end
