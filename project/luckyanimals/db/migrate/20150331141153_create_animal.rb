class CreateAnimal < ActiveRecord::Migration
  def change
    create_table :animals do |t|
      t.string :name
      t.integer :gender
      t.datetime :birthday
      t.integer :species
      t.integer :size
      t.boolean :castraded
      t.string :coat_color
      t.string :eyes_color
      t.boolean :adopted
      t.timestamps null: false
    end
  end
end
