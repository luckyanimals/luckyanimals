class AddUserRefToAnimals < ActiveRecord::Migration
  def change
    add_reference :animals, :user, index: true
    add_foreign_key :animals, :users
  end
end
