class AddAnimalRefToPosts < ActiveRecord::Migration
  def change
  	add_reference :posts, :animal, index: true
    add_foreign_key :posts, :animals
  end
end
