class AddPostRefToPostAttachments < ActiveRecord::Migration
  def change
  	add_reference :post_attachments, :post, index: true
    add_foreign_key :post_attachments, :posts
  end
end
