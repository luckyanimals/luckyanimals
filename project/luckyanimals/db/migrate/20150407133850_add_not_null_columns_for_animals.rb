class AddNotNullColumnsForAnimals < ActiveRecord::Migration
  def change
  	change_column_null :animals, :name, false
  	change_column_null :animals, :gender, false
  	change_column_null :animals, :birthday, false
  	change_column_null :animals, :species, false
  	change_column_null :animals, :size, false
  	change_column_null :animals, :castraded, false
  	change_column_null :animals, :coat_color, false
  	change_column_null :animals, :eyes_color, false
  	change_column_null :animals, :adopted, false
  	change_column_null :animals, :user_id, false

  end
end
